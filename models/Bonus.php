<?php

namespace app\models;

use Yii;
// Code 4 debriefing fields
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
//


/**
 * This is the model class for table "bonus".
 *
 * @property integer $id
 * @property integer $userId
 * @property integer $reasonId
 * @property integer $amount
 * @property string $created_at
 * @property string $update_at
 * @property integer $created_by
 * @property integer $update_by
 */
class Bonus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bonus';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'userId', 'reasonId', 'amount'], 'required'],
			['amount', 'compare', 'compareValue' => 100, 'operator' => '>'],
			['amount', 'compare', 'compareValue' => 1000, 'operator' => '<'],
            [['id', 'userId', 'reasonId', 'amount', 'created_by', 'update_by'], 'integer'],
            [['created_at', 'update_at'], 'safe'],
			//, 'created_at', 'update_at', 'created_by', 'update_by'
        ];
    }

	public function getReasonIdItem()
    {
        return $this->hasOne(Bonusreason::className(), ['id' => 'reasonId']);
    }


	// Code 4 debriefing fields
	/*public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => 'yii\behaviors\TimestampBehavior',
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
    }*/

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'User ID',
            'reasonId' => 'Reason ID',
            'amount' => 'Amount',
            'created_at' => 'Created At',
            'update_at' => 'Update At',
            'created_by' => 'Created By',
            'update_by' => 'Update By',
        ];
    }

}
