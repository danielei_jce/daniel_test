<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use app\models\Bonus;

/**
 * This is the model class for table "lead".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $notes
 * @property integer $status
 * @property integer $owner
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class Lead extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lead';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
		$rules = []; 
		$stringItems = [['notes'], 'string'];
		$integerItems  = ['status', 'created_at', 'updated_at', 'created_by', 'updated_by'];		
		if (\Yii::$app->user->can('updateLead')) {
			$integerItems[] = 'owner';
		}
		$integerRule = [];
		$integerRule[] = $integerItems;
		$integerRule[] = 'integer';
		$ShortStringItems = [['name', 'email', 'phone'], 'string', 'max' => 255]; 
		$rules[] = $stringItems;
		$rules[] = $integerRule;
		$rules[] = $ShortStringItems;		
		return $rules;
    }

    /**
     * @inheritdoc
     */

    /**
     * Defenition of relation to user table
     */    

	 public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => 'yii\behaviors\TimestampBehavior',
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
    }

    /**
     * Defenition of relation to user table
     */ 	
	
	public function getUserOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner']);
    }
	
	public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }	

	public function getUpdateddBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }	
	
    /**
     * Defenition of relation to status table
     */  
 	
 
	public function getStatusItem()
    {
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }
	
	public function afterSave($insert,$changedAttributes)
    {
        $return = parent::afterSave($insert, $changedAttributes);

      

		 if (($this->status == 3)){
		 
		 Yii::$app->db->createCommand()
					->insert('bonus', [
					'userId' => $this->owner,
					'amount' => 100,
					])->execute();
			
			
			/*$db = \Yii::$app->db;
			$db->createCommand()->delete('bonus',
				['userId' => $this->owner])->execute();
			$auth->assign(amount, 100);*/}
			
			//$this->status_id = 1 : $this->status_id;
			
			/*Yii::$app->db->createCommand("UPDATE table Bonus amount=:100, WHERE leadId=user:id")
					->bindValue('user:id', leadId)
					->execute();
			/*Yii::$app->db->createCommand()
					->insert('Bonus', [
					'amount' => 100,
					])->execute();	*/	
					

			//$model->amount = 100;
		  // $model->amount = 100;
        return $return;
    }		
		

	public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'notes' => 'Notes',
            'status' => 'Status',
            'owner' => 'Owner',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
}
